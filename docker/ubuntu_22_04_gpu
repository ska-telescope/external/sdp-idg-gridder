ARG NVIDIA_VERSION=12.6.2

FROM nvidia/cuda:${NVIDIA_VERSION}-devel-ubuntu22.04 AS build

RUN export DEBIAN_FRONTEND=noninteractive && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
    libboost-all-dev libboost-test-dev g++ cmake libfftw3-dev liblapacke-dev \
    python3 python3-pip git file ninja-build pkg-config

ADD . /idg

RUN cd /idg && \
    # Unshallowing is needed for determining the IDG version.
    if $(git rev-parse --is-shallow-repository); then git fetch --unshallow; fi && \
    mkdir build && \
    cd build && \
    cmake .. -G Ninja \
      -DCMAKE_INSTALL_PREFIX=/usr \
      "-DCMAKE_LIBRARY_PATH=/usr/local/cuda/compat;/usr/local/cuda/lib64" \
      "-DCMAKE_CXX_FLAGS=-isystem /usr/local/cuda/include" \
      -DBUILD_TESTING=On -DBUILD_LIB_CUDA=On -DBUILD_PACKAGES=On && \
    ninja package

# IDG needs the 'devel' image, since the 'runtime' image does not have nvcc.
FROM nvidia/cuda:${NVIDIA_VERSION}-devel-ubuntu22.04

# The metadata should include the IDG version.
ARG IDG_VERSION

# Do not hardcode a specific author. Let a --build-arg supply it, instead.
ARG AUTHOR

# https://developer.skao.int/en/latest/tools/containers/containerisation-standards.html#labels
# has a list that contains the required labels.
LABEL \
    author="${AUTHOR}" \
    description="IDG library with CUDA support" \
    license="GPL3.0" \
    int.skao.team="Team Schaap" \
    int.skao.application="idg" \
    int.skao.version=${IDG_VERSION} \
    int.skao.repository="https://gitlab.com/ska-telescope/sdp/ska-sdp-func-idg"

COPY --from=build /idg/build/idg-*.deb /idg/
COPY --from=build /idg/build/bin/* /idg/bin/
RUN apt-get update && \
    apt install -y /idg/*.deb && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
